const Course = require("../models/Course.js");
const auth = require("../auth.js");

// Create a new course
/*
	Business Logic:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new Course to the database
*/

module.exports.addCourse = (reqBody) => {
  let newCourse = new Course({
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price,
  });

  return newCourse.save().then((course, err) => {
    if (err) {
      return false;
    } else {
      return true;
    }
  });
};

// Retrieving all courses

/*
	Business Logic:
	1. Retrieve all the courses from the database
*/

module.exports.getAllCourses = () => {
  return Course.find({}).then((result) => {
    return result;
  });
};

// Retrieving all Active Courses

/*
	Business Logic:
	1. Retrieve all the courses from the database with the property of "isActive" to true
*/

module.exports.getActiveCourses = () => {
  return Course.find({ isActive: true }).then((result) => {
    return result;
  });
};

// Retrieving a Specific Course

/*
	Business Logic:
	1. Retrieve the course that matches the course ID provided from the URL
*/
// reqParams = req.params(url) = "/:courseId"

module.exports.getCourse = (reqParams) => {
  return Course.findById(reqParams.courseId).then((result) => {
    return result;
  });
};

// Updating a Course

/*
	Business Logic:
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
	2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
*/
// course = data.course = req.body
// paramsId = data.params = req.params = /:courseId

module.exports.updateCourse = (course, paramsId) => {
  // specify the field properties that needs to be updated
  let updatedCourse = {
    name: course.name,
    description: course.description,
    price: course.price,
  };

  // findbyIdAndUpdate(id, updatedContent)

  return Course.findByIdAndUpdate(paramsId.courseId, updatedCourse).then(
    (result, err) => {
      if (err) {
        return false;
      } else {
        return true;
      }
    }
  );
};
