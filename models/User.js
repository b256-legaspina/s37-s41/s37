// Dependencies
const mongoose = require("mongoose");

// Schema
const userSchema = new mongoose.Schema ({

	firstName: {
		type: String,
		required: [true, `First name is required`]
	},

	lastName: {
		type: String,
		required: [true, `Last name is required`]
	},

	email: {
		type: String,
		required: [true, `email address is required`]
	},

	password: {
		type: String,
		required: [true, `please input password`]
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	mobileNo: {
		type: String,
		required : [true, `please input number`]
	},

	enrollments : [
	{
		courseId: {
			type: String,
			required: [true, `ID is required`]
		},

		enrolledOn: {
			type: Date,
		default: new Date()
		},

		status: {
			type: String,
		default: "Enrolled"
		}
	}	
		]
});

module.exports = mongoose.model("User", userSchema);